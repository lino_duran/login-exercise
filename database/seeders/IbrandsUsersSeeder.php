<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class IbrandsUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('ibrands_users')->insert([
            'name' => 'Paula',
            'email' => 'paula.vandrell@ibrands.com',
            'password' => Hash::make('password'),
            'attempts'=>0,
            'blocked'=>false
        ]);

        DB::table('ibrands_users')->insert([
            'name' => 'Jesus',
            'email' => 'jesus.lorenzo@ibrands.com',
            'password' => Hash::make('password'),
            'attempts'=>0,
            'blocked'=>false
        ]);

        DB::table('ibrands_users')->insert([
            'name' => 'Jorge',
            'email' => 'jorge.fernandez@ibrands.com',
            'password' => Hash::make('password'),
            'attempts'=>0,
            'blocked'=>false
        ]);

        DB::table('ibrands_users')->insert([
            'name' => 'Lino',
            'email' => 'lino.duran@ibrands.com',
            'password' => Hash::make('password'),
            'attempts'=>0,
            'blocked'=>false
        ]);


        // some random
        for($i=0;$i<10;$i++){
            DB::table('ibrands_users')->insert([
                'name' => Str::random(10),
                'email' => Str::random(10).'@ibrands.com',
                'password' => Hash::make('password'),
                'attempts'=>0,
                'blocked'=>false
            ]);
        }
    }
}
