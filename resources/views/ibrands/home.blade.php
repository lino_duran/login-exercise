@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">{{ __('Bienvenido') }} {{ $user->name}}</div>

                <div class="card-body">
                    
                    <div class="alert alert-success" role="alert">        
                        {{ __('Estas logueado!') }}
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <a href="{{ route('logout') }}" class="btn btn-primary">
                                {{ __('Cerrar sesion') }}
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Cambio de contraseña') }}</div>

                <div class="card-body">

                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @elseif (session('error'))                        
                        <div class="alert alert-success" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('changePassword') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña actual') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new-password-1" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña nueva') }}</label>

                            <div class="col-md-6">
                                <input id="new-password-1" type="password" class="form-control @error('new-password-1') is-invalid @enderror" name="new-password-1" required autocomplete="current-password">

                                @error('new-password-1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new-password-2" class="col-md-4 col-form-label text-md-right">{{ __('Repita contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="new-password-2" type="password" class="form-control @error('new-password-2') is-invalid @enderror" name="new-password-2" required autocomplete="current-password">

                                @error('new-password-2')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cambiar') }}
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
