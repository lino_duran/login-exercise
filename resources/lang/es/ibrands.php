<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'notvaliduser' => 'Usuario o contraseña no valido.',
    'blocked' => 'El usuario esta bloquedo.',
    'differentnewpasswords' => 'Las contraseñas no coinciden',
    'notvalidpassword' => 'La contraseña introducida no es correcta',
    'passwordupdated' => 'La contraseña se ha actualizado correctamente',
    'passwordupdateerror' => 'No se ha podido actualizar la contraseña. Por favor vuelva a intentarlo. Si el error persiste contacte con el administrador.',

];