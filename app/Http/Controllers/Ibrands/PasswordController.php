<?php

namespace App\Http\Controllers\Ibrands;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Models\IbrandsUser;
use Exception;
use Illuminate\Support\Facades\Hash;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Log;

class PasswordController extends Controller
{

    //claves para i18n
    const MSG_ERROR_NEW_PASS = 'ibrands.differentnewpasswords';
    const MSG_ERROR_PASS = 'ibrands.notvalidpassword';

    //
    const MSG_PASS_UPDATED = 'ibrands.passwordupdated';
    const MSG_PASS_UPDATE_ERROR = 'ibrands.passwordupdateerror';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('ibrands.auth');
    }

    /**
     * Maneja la peticion de cambio de password(POST)
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changePassword(Request $request)
    {
        $credentials = $request->validate([
            'password' => 'required|string',
            'new-password-1' => 'required|string',
            'new-password-2' => 'required|string',
        ]);

        $user = $request->session()->get('user');

        $validatorBag = [];

        // comprobamos si la password actual coincide
        if (!Hash::check($credentials['password'], $user->password)) {
            // enviamos feedback al frontend
            $validatorBag = [
                'password' => __(self::MSG_ERROR_PASS),
            ] +  $validatorBag;
        }

        // comprobamos que la nuevas password sean iguales
        if ($credentials['new-password-1'] != $credentials['new-password-2']) {
            $validatorBag = [
                'new-password-1' => __(self::MSG_ERROR_NEW_PASS),
                'new-password-2' => __(self::MSG_ERROR_NEW_PASS),
            ] +  $validatorBag;
        }

        if (!empty($validatorBag)) {
            // si hay errores los escalamos
            throw ValidationException::withMessages($validatorBag);
        }

        try {
            $this->updateUser($user->id, $credentials['new-password-1'], $request);
            return redirect(RouteServiceProvider::HOME)->with('success', __(self::MSG_PASS_UPDATED));
        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            return redirect(RouteServiceProvider::HOME)->with('error', __(self::MSG_PASS_UPDATE_ERROR));
        }
    }

    /**
     * Actualizacion de la contraseña de 
     *  @param  $userId
     *  @param  $password
     *  @return void
     * 
     *  @throws Exception
     */
    protected function updateUser($userId, string $password, Request $request)
    {
        $user = IbrandsUser::find($userId);
        if (is_null($user)) {
            throw new Exception("Intentando actualizar un usuario inexistente con id: " . $userId);
        }
        $user->password = Hash::make($password);
        $user->save();
        $request->session()->put('user', $user);
    }
}
