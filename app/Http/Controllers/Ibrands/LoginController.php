<?php

namespace App\Http\Controllers\Ibrands;

use App\Http\Controllers\Controller;
use App\Models\IbrandsUser;
use App\Providers\RouteServiceProvider;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Events\IbrandsLoginFailed;
use App\Events\IbrandsLoginSucceded;

class LoginController extends Controller
{
    /*
        Login Controller
        Maneja la autenticacion y redirige a la pantalla de inicio
    */


    //claves para i18n
    const MSG_ERROR_EMAIL_PASS ='ibrands.notvaliduser';
    const MSG_ERROR_BLOCKED ='ibrands.blocked';

    
    /**
     * Simplemente muestra la pantalla de login
     */
    public function showLoginForm(Request $request)
    {
        return view('ibrands.login');
    }

    /**
     * Vacia la session y redirige al login
     */
    public function logout(Request $request)
    {
        // reseteamos la sesion
        $request->session()->forget('user');
        $request->session()->invalidate();
        return redirect('login');
    }
    

    /**
     * Maneja la peticion de login (POST)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        

        $credentials = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $user = IbrandsUser::where('email', $credentials['email'])
                            ->first();

        
        if(is_null($user))
        {
            throw ValidationException::withMessages([
                'email' => [__(self::MSG_ERROR_EMAIL_PASS)],
            ]);
        }
        else if( !Hash::check($credentials['password'], $user->password) )
        {
            // lanzamos el evento
            IbrandsLoginFailed::dispatch($user);
            // y enviamos feedback al frontend
            throw ValidationException::withMessages([
                'email' => [__(self::MSG_ERROR_EMAIL_PASS)], // usamos el mismo mensaje que en le primer caso por motivos de seguridad
            ]);
        }
        else if ($user->blocked) 
        {
            throw ValidationException::withMessages([
                'email' => [__(self::MSG_ERROR_BLOCKED)], // si las credenciales son correctas pero esta bloquedo.... mala  suerte
            ]);
        }
                
        return $this->succeedLogin($request, $user);        
    }


    /**
     * Acciones a completar en la autenticacion exitosa
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    protected function succeedLogin(Request $request, IbrandsUser $user)
    {
         // reseteamos la sesiom
        $request->session()->regenerate();
        $request->session()->put('user', $user);
         // lanzamos el evento
        IbrandsLoginSucceded::dispatch($user);
        return redirect(RouteServiceProvider::HOME);
    }


}
