<?php

namespace App\Listeners;

use App\Events\IbrandsLoginSucceded;
use App\Models\IbrandsUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IbrandsResetAttempts
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IbrandsUser $user
     * @return void
     */
    public function handle(IbrandsLoginSucceded $event)
    {
        $user= $event->getUser();
        $user->attempts=0;
        $user->save();
    }
}
