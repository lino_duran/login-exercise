<?php

namespace App\Listeners;

use App\Events\IbrandsLoginFailed;
use App\Models\IbrandsUser;

class IbrandsBlockUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IbrandsUser $user
     * @return void
     */
    public function handle(IbrandsLoginFailed $event)
    {
        $user= $event->getUser();
        $user->attempts+=1;

        if($user->attempts >= env('MAX_LOGIN_ATTEMPTS', 3)){
            $user->blocked=true;
        }
        $user->save();
    }
}
