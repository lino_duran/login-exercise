<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\IbrandsUser;

class IbrandsLoginSucceded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @property IbrandsUser
     */
    private $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(IbrandsUser $user)
    {
        $this->user=$user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    /**
     * @return IbrandsUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
